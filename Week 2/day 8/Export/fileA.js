//export
let students = ["gusti", "ardi"];

//the first way
// module.exports = {
//   students,
// };

// //or
// module.exports.students = students;

//the second way
exports.students = ["gusti", "ardi"];
